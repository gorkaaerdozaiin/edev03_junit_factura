package edEV03_jUnit_Factura;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class ProductoTest {
	
	@Test
	void testNuevo() {
		Producto p = new Producto("pan", 0.50f, 1);
		assertTrue(p.getNombre() == "pan");
	}
	
	@Test
	void testPrecioTotal() {
		Producto p = new Producto("nocilla", 2f, 2);
		assertTrue(p.precioTotal() == p.getPrecio() * p.getCantidad());
	}

}
