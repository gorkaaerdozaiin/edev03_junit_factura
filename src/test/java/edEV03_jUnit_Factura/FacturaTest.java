package edEV03_jUnit_Factura;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class FacturaTest {

	@Test
	void testTotalFactura() {
		Factura f = new Factura();
		f.meterProducto(new Producto("alubias", 1, 4));
		f.meterProducto(new Producto("lentejas", 1, 6));
		float actual = f.totalFactura();
		int expected = 4 * 1 + 6 * 1;
		assertEquals(actual, expected);
	}
	
	@Test
	void testAplicarIva() {
		Factura f = new Factura();
		f.meterProducto(new Producto("alubias", 1, 4));
		f.meterProducto(new Producto("lentejas", 1, 6));
		f.aplicarIva(21);
		float actual = f.totalFactura();
		int expected = 4 * 1 + 6 * 1;
		assertEquals(actual, expected);
	}

}
