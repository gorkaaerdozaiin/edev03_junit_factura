package edEV03_jUnit_Factura;

import java.util.ArrayList;

public class Factura {

	private ArrayList<Producto>productos = new ArrayList<Producto>();
	
	public void meterProducto(Producto p) {
		productos.add(p);
	}
	
	public float totalFactura() {
		float precio = 0;
		for (Producto producto : productos) {
			precio += producto.precioTotal();
		}
		return precio;
	}
	
	public float aplicarIva(float iva) {
		float ivaFactura = 0;
		ivaFactura = totalFactura() + totalFactura() * iva;
		return ivaFactura;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}
	
}
